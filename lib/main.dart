import 'package:flutter/material.dart';
import 'package:flutter_application_1/splashScreen.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({key});
 
  @override
  Widget build(BuildContext context) {
    return MaterialApp( 
      debugShowCheckedModeBanner: false,
      title: 'ChatGPT Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen1(),
    );
  }
}
