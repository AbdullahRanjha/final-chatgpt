import 'package:flutter/material.dart';
import 'package:flutter_application_1/contact_us.dart';
import 'package:flutter_application_1/privacy_policy.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewHomePage extends StatefulWidget {
  const NewHomePage({Key key}) : super(key: key);

  @override
  State<NewHomePage> createState() => _NewHomePageState();
}

class _NewHomePageState extends State<NewHomePage> {
  InAppWebViewController webViewController;
  PullToRefreshController refreshController;
  String url = "https://chat.openai.com/auth/login";
  String successUrl = 'https://chat.openai.com/chat';
  var isLoading = false;
  bool _isLoggedIn = false;
  BannerAd _bannerAd;
  bool _isAdloaded = false;
  bool _isLoading = true;
  final _bannerAdUnitId = 'ca-app-pub-5760470859736327/2691419973';
  final _intrestialAdUnitId = 'ca-app-pub-5760470859736327/3812929954';
  InterstitialAd interstitialAd;

  @override
  void initState() {
    _initBannerAd();
    checkLoginStatus();
    _interestialAd();
  }

  // Future<void> _launchUrl() async {
  //   if (!await launchUrl(_url1)) {
  //     throw 'Could not launch $_url1';
  //   }
  // }

  void checkLoginStatus() async {
    bool isLoggedIn = await SharedPreferences.getInstance().then((value) {
      return value.getBool('isLoggedIn') ?? false;
    });

    setState(() {
      _isLoggedIn = isLoggedIn;
    });
  }

  void logIn() async {
    // Store login status in shared preferences
    await SharedPreferences.getInstance().then((value) {
      value.setBool('isLoggedIn', true);
    });

    setState(() {
      _isLoggedIn = true;
    });
  }

  void logOut() async {
    // Store login status in shared preferences
    await SharedPreferences.getInstance().then((value) {
      value.setBool('isLoggedIn', false);
    });

    setState(() {
      _isLoggedIn = false;
    });
  }

  // Future<void> share() async {
  //   await FlutterShare.share(
  //       title: 'Share',
  //       chooserTitle: 'Link',
  //       linkUrl: 'https://www.facebook.com/');
  // }

  void _interestialAd() {
    InterstitialAd.load(
        adUnitId: _intrestialAdUnitId,
        request: AdRequest(),
        adLoadCallback: InterstitialAdLoadCallback(onAdLoaded: (ad) {
          interstitialAd = ad;
          interstitialAd.show();

          interstitialAd.fullScreenContentCallback = FullScreenContentCallback(
              onAdFailedToShowFullScreenContent: ((ad, error) {
            ad.dispose();
            interstitialAd.dispose();
            debugPrint(error.message);
          }), onAdDismissedFullScreenContent: (ad) {
            ad.dispose();
            interstitialAd.dispose();
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => Screen1()));
          });
        }, onAdFailedToLoad: (error) {
          debugPrint(error.message);
        }));
  }

  _initBannerAd() {
    _bannerAd = BannerAd(
      size: AdSize.banner,
      adUnitId: _bannerAdUnitId,
      listener: BannerAdListener(
          onAdLoaded: (ad) {
            setState(() {
              _isAdloaded = true;
            });
          },
          onAdFailedToLoad: ((ad, error) {})),
      request: AdRequest(),
    );
    _bannerAd.load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: _homePageController.,
      bottomNavigationBar: Container(
        alignment: Alignment.bottomCenter,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _isAdloaded
                ? Container(
                    height: _bannerAd.size.height.toDouble(),
                    width: _bannerAd.size.width.toDouble(),
                    child: AdWidget(ad: _bannerAd),
                  )
                : SizedBox(),
          ],
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () {
                webViewController.reload();
              },
              icon: Icon(Icons.refresh)),
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 1,
                child: Row(
                  children: [
                    Icon(
                      Icons.privacy_tip_outlined,
                      color: Color.fromARGB(255, 193, 184, 184),
                    ),
                    Text('   Privacy Policy')
                  ],
                ),
              ),
              PopupMenuItem(
                value: 2,
                // enabled: true,
                child: Row(
                  children: [
                    Icon(
                      Icons.contact_support,
                      color: Color.fromARGB(255, 193, 184, 184),
                    ),
                    Text('   Contact Us')
                  ],
                ),
              ),
            ],
            onSelected: (value) async {
              if (value == 1) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PrivacyPage(),
                    ));
              } else if (value == 2) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => contacts(),
                    ));
              }
            },
          ),
        ],
        backgroundColor: Color.fromARGB(255, 50, 55, 67),
        title: Center(child: Text('         GPT Chat Android')),
      ),
      body: Column(
        children: [
          Expanded(
              child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                  child: _isLoggedIn
                      ? InAppWebView(
                          onLoadStart: (controller, url) {
                            setState(() {
                              isLoading = true;
                            });
                          },
                          onLoadStop: (controller, url) {
                            setState(() {
                              isLoading = false;
                            });
                          },
                          onWebViewCreated: (controller) =>
                              webViewController = controller,
                          initialUrlRequest: URLRequest(url: Uri.parse(url)),
                        )
                      : Container(
                          child: InAppWebView(
                            onLoadStart: (controller, url) {
                              setState(() {
                                isLoading = true;
                              });
                            },
                            onLoadStop: (controller, url) {
                              setState(() {
                                isLoading = false;
                              });
                            },
                            onWebViewCreated: (controller) =>
                                webViewController = controller,
                            initialUrlRequest:
                                URLRequest(url: Uri.parse(successUrl)),
                          ),
                        )),
              Visibility(
                  visible: isLoading,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.red[300]),
                  ))
            ],
          ))
        ],
      ),
    );
  }
}
