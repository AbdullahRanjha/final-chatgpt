import 'package:flutter/material.dart';
// import 'package:flutter_application_1/home_page.dart';
import 'package:easy_splash_screen/easy_splash_screen.dart';
import 'package:flutter_application_1/newHomePage.dart';

class SplashScreen1 extends StatefulWidget {
  const SplashScreen1({key});

  @override
  State<SplashScreen1> createState() => _SplashScreen1State();
}

class _SplashScreen1State extends State<SplashScreen1> {
  @override
  Widget build(BuildContext context) {
    return EasySplashScreen(
      backgroundColor: Colors.white,
      logo: Image(image: AssetImage('assets/logo.png')),
      title: Text(
        'ChatGPT Mobile',
        style: TextStyle(
            color: Colors.black, fontStyle: FontStyle.normal, fontSize: 25),
      ),
      showLoader: true,
      durationInSeconds: 3,
      loadingText: Text('Please wait...'),
      navigator: NewHomePage(),
      loaderColor: Colors.red, 
      logoWidth: 100,
    );
  }
}
