import 'package:flutter/material.dart';
import 'package:contactus/contactus.dart';

class contacts extends StatefulWidget {
  const contacts({Key key}) : super(key: key);

  @override
  State<contacts> createState() => _contactsState();
}

class _contactsState extends State<contacts> {
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.,
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 50, 55, 67),
        title: Center(child: Text('Contact Us')),
      ),
      body: ListView(children: [
        ContactUs(
          companyName: 'INNOPPIA Pakistan',
          textColor: Color.fromARGB(255, 0, 0, 0),
          cardColor: Color.fromARGB(255, 253, 253, 253),
          companyColor: Color.fromARGB(255, 0, 0, 0),
          taglineColor: Color.fromARGB(255, 0, 0, 0),
          email: 'sales@innoppia.pk',
          phoneNumberText: '+92554805360',
          emailText: 'sales@innoppia.pk',
          phoneNumber: '+92554805360',
          website: 'https://innoppia.pk/',
          websiteText: 'https://innoppia.pk/',
          dividerColor: Colors.black,
          avatarRadius: 80,
          companyFontSize: 35,
          logo: AssetImage('assets/logo.png'),
          // facebookHandle:
          //     'https://www.facebook.com/profile.php?id=100008875542941',
              
        ),
      ]),
    );
  }
}
